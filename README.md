# Processing and Storage Component for IoT Telemetry and Monitoring System
Processing and storage component of a distributed telemetry and monitoring system for fuel tanks, coming from a university project done during SOA classes.

## General info
The server that we used is a server programmed specially for this task, using *spring boot*.

## Used Technologies
* Docker Engine Client 19.03.5
* Docker Engine Server 19.03.5
* docker-compose 1.21.2
* MySQL 8.0.19
* Redis 5.0.9
* Metabase v0.35.4
* Nginx 1.19.0
* trabajoIntegradorSOA2020 0.0.1
* Nodered 1.0.4

## Architecture
The architecture of the project is the one shown at the figure:

![Arch image](./images/architecture.jfif)

## Setup
### Controller
The controller section was implemented with Nodered and simulated with an Arduino. Both of them communicate through the **modbus**, which implementation for the Arduino can be found at *arduino/code.ino*, and the link to the used library is [this](https://github.com/andresarmento/modbus-arduino).

Take into account the settings of **modbus**, the **mqtt broker** and  the **backend** IP at the Nodered flow, which can be found at *nodered-controller/flows.json*.

And for those who are interested, the connections used at the Arduino to simulate the entering data was:  
 ![Connections Arduino](./images/`Diagrama conexiones.png`)

#### Modbus registers table
Data |Register Number
--- | ---
Temperature | 0
Humidity | 1
TotalLevel | 2
WaterLevel | 3
Density | 4
Density_decimal | 5

### Dashboards
The dashboards where implemented also with Nodered, and their flows can be found at *nodered-dashboard/flows.json*. 

In order to change the url of the dashboard so it can be accessed through the address of the server, you ought to uncomment the lines:  

`httpAdminRoot: '/admin'`  

`ui: { path: "" }`  

from the configuration file of Nodered *settings.js*.

Take care of the settings of **mqtt broker** to be properly stablished at the Nodered flow before deploying it.

### Backend Springboot Server
The backend was done with **springboot** and **springboot security**, and other dependencies that are declared at the pom.xml that is located at */backend/*. If you want to modify the code,after that, to recreate the jar file you should use the command:  

`mvn clean package`  

If you want to debug the code, you can do it with:  

`mvn spring-boot:run`  

Take into consideration that the spring servers expects an TLS certificate at the */home/certs/* folder. The specific properties of this TLS certificates, as specified from the base server, are declared below, at the **Example of setting SSL certificate with Let'sEncrypt** section.

For more information about the API, see the docs *docs/Documentación de la API - TP Integrador SOA 2020.pdf*(/docs/Documentación de la API - TP Integrador SOA 2020.pdf).

### Backend Docker
Make sure you have docker running on your environment.

After downloading the *backend/Docker/ directory, you will need some configuration previous at running the project.

In order for the server to run, it expects an SSL certificate stored in PKCS12  repository format, protected by the password **IUA-SOA2020**, that should go in the directory *backend/Docker/spring/certificates*. Don't forget to give to *docker-compose* read access to the SSL certificate, otherwise it will fail at start-up time. See below if you want to see how to establish a SSL certificate from Let'sEcrypt.

Metabase is also expeting an SSL certificate. But as it runs with a Jetty webserver, the format it expects the TLS certificate to be is not PKCS12 but in a JKS (Java KeyStore) repository. This certificate should be called *sslcert.jks* (unless you change the configuration at *backend/Docker/metabase/metabase_conf.env*), should have as private key and storage password the pattern **IUA-SOA2020**, and is to be placed at *backend/Docker/config/certs*. See below if you want to see a brief explanation and the commands to establish the certificate from Let'sEncrypt with jks format.

Finally, the Nginx reverse proxy, that takes care of redirecting urls so ports don't ought to be specified, also uses TLS certificates to work (this is actually the only one that really needs tls certificates, but because of lack of time, we weren't able to re-config the spring server so to not to need to configure HTTPS for him). It expects two files: *fullchain.pem* and *privkey.pem*, to be at *backend/Docker/config/certs*. See below if you want a more detailed explanation of how to do that. And take into account that in order to make it work, you should change the **server_name** attribute located at *backend/Docker/nginx/conf.d/nginx.conf* to **your domain name**.

Now, after the certificates are in place, and nginx is correctly configured, simply build the images and run the services with:  

`docker-compose up --build -d`  

Finally, after the system being up and running, metabase will be running  and saving everithing at the schema *metabase* on the MySQL database.

### Example of setting SSL certificate with Let'sEncrypt
Here you have an example guide in case you're interested in using a Let'sEcnrypt certificate. We will need certbot installed on the server, in order to configure our *free* certificate. Make sure you have it before proceeding.
#### To run certbot
`sudo certbot certonly -a standalone -d [domain name]`
#### To use the certificate on SpringBoot server
Spring-Boot does not support PEM files generated by Let’s Encrypt. Spring Boot supports PKCS12 extension. Using OpenSSL, we convert our certificate and private key to PKCS12.

1. Go to */etc/letsencrypt/live/[domain name]*
2. Convert the keys to PKCS12 using OpenSSL as follows:  

`openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out [name of keystore] -name [key alias] -passout pass:[password]`

Our server, if used as it is, uses specific parameter values. To run just copy and execute the next command:

`openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out springkeystore.p12 -name tomcat -passout pass:IUA-SOA2020`

3. Move the file *springkeystore.p12* to the directory *backend/Docker/spring/certificates*
4. Change mod of *springkeystore.p12* file so docker-compose has rights to read it:  

`sudo chmod 550 backend/Docker/spring/certificates/springkeystore.p12`

### Metabase with HTTPS
Assuming you already have the SSL certificate from Let'sEncrypt, and that you have followed the *Example of setting SSL certificate with Let'sEncrypt* that's below, the nexts steps need to be done in order to get metabase running with HTTPS.

1. Go to *backend/Docker/spring/certificates* (where the SSL certificate in a PKCS12 repository should be palced).
2. Convert the keys to JKS using the *keytool* utility from Java distributions:

run
` keytool -importkeystore -destkeypass IUA-SOA2020 -deststorepass IUA-SOA2020 -destkeystore sslcert.jks -srckeystore springkeystore.p12 -srcstoretype PKCS12 -srcstorepass IUA-SOA2020 -alias tomcat

If you want to change the repository or private key passwords, you should change the *-destkeypass* and/or *-deststorepass* parameters.

3. Move the file *sslcert.jks* to *backend/Docker/config/certs* directory.
4. Change mod of *sslcert.jks* file so docker-compose has rights to read it:  

`sudo chmod 550 backend/Docker/config/certs/springkeystore.p12`


### Configuration of Nginx
So, supposing that the SSL .pem certificates repositorys' are at */etc/letsencrypt/live/[domain name]* (check *Example of setting SSL certificate with Let'sEncrypt* that's below), the steps to follow are simply:

1. Move *fullchain.pem* and *privkey.pem* to *backend/Docker/config/certs*.
2. Change mod of files so docker-compose has rights to read them:
`sudo chmod 550 backend/Docker/config/certs/*`

Then, to indicate to Nginx the domain name to use:

1. Open the file *backend/Docker/nginx/conf.d/nginx.conf*.
2. Change the variable *server_name* at the HTTPS server definition to the corresponding domain name.

Take into account that any other modifications to the configuration to nginx that you would want to do should be done at this file, after what the nginx-soa service should be re-builded.

### Check if the server is up
To check if the spring server is up, you can run the next curl command, with the  corresponing [parameters] replaced (after doing `docker-compose up --build -d`).  

`curl -X POST -H 'Content-type:application/json' -d '{"username":"admin", "password":"CRUC-IUA-UNDEF"}' https://your_domain_name/backend/login -v -k`

## Check the API
To see the documentation created automatically by Swagger, you can go to *https://your_domain_name/backend/swagger-ui.html*.

## University
CRUC-IUA-UNDEF

## Authors:
* ALESSANDRO Giuliano
* GOMEZ Nicolás
* GUERRA Ivan
* PEREZ SARDI Walter Gabriel
* PEREYA Agustin Ezequiel.
