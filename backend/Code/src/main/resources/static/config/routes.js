angular.module('soa2020')

.config(function($locationProvider, $routeProvider, $localStorageProvider, $httpProvider) {
	$locationProvider.hashPrefix('!');
	
	$localStorageProvider.setKeyPrefix('soa2020');
	
	$httpProvider.defaults.withCredentials = true;
	
	$httpProvider.interceptors.push('APIInterceptor');
	
	
	$routeProvider
	
	.when('/',{
		templateUrl: 'views/main.html',
		controller:'main'
	})
	.when('/water',{
		templateUrl: 'views/water.html',
		controller:'water'
	})
	.when('/tanks',{
		templateUrl: 'views/tanks.html',
		controller:'tanks'
	})
	.when('/gov',{
		templateUrl: 'views/gov.html',
		controller:'gov'
	})
	.when('/temperatures',{
		templateUrl: 'views/temperatures.html',
		controller:'temperatures'
	})	
	.otherwise({
		redirectTo: '/'
	})
});

