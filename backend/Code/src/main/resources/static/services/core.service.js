angular.module('soa2020').factory('coreService',function($http,URL_BASE){
	return {
			
		loginJwt: function(user) {
			var req = {
				method: 'POST',
				url: URL_BASE+'login',
				headers : { 'Content-Type': 'application/json' },
				data: user
			};
			return $http(req);
		}
	}
});