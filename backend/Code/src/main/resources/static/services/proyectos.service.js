angular.module('soa2020').factory('proyectosService',function($http, URL_API_BASE){
	return {
		list:function() {
			return $http.get(URL_API_BASE+"proyectos");
		}
	}
});