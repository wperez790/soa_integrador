package ar.edu.iua.trabajoIntegradorSOA2020.business;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.User;

public interface IUserBusiness {
    User create(User user) throws BusinessException;
    //  Cambiar usuario segun el token que se paso y la clave nueva
    User changePassword(String auth, String password) throws BusinessException;
}
