package ar.edu.iua.trabajoIntegradorSOA2020.persistence;

import ar.edu.iua.trabajoIntegradorSOA2020.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<User, Integer> {}