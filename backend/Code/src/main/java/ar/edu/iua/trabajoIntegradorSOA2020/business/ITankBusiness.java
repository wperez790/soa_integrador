package ar.edu.iua.trabajoIntegradorSOA2020.business;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;

public interface ITankBusiness {
    Tank getOne(int id) throws BusinessException, NotFoundException;
    Tank addTank(Tank tank) throws BusinessException;
    Tank modifyTank(int id, Tank tank) throws BusinessException;
}