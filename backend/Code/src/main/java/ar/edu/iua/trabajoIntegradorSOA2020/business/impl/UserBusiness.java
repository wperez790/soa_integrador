package ar.edu.iua.trabajoIntegradorSOA2020.business.impl;

import ar.edu.iua.trabajoIntegradorSOA2020.business.IUserBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.User;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.UserRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.security.JWTUtils;

import ar.edu.iua.trabajoIntegradorSOA2020.security.SecurityConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserBusiness implements IUserBusiness {

    private static Logger log = LoggerFactory.getLogger(UserBusiness.class);

    @Autowired
    UserRepository userDAO;

    @Autowired
    JWTUtils jwtUtils;

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserBusiness() {}

    @Override
    public User create(User user) throws BusinessException {
        try {
            //  Codificar la clave segun el algoritmo establecido en la configuracion
            String passwordHashed = passwordEncoder.encode(user.getPassword());
            user.setPassword(passwordHashed);
            return userDAO.save(user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    @Override
    public User changePassword(String auth, String password) throws BusinessException {
        try {
            //  Obtener el usuario del token.
            //  Aclaracion: si llegamos hasta aca, el token ya ha sido
            //  verificado por los filtros correspondientes.
            String jwtToken = auth.replace(SecurityConstants.BEARER_TOKEN_PREFIX + " ", "");  //  Limpiar encabezado
            String username = jwtUtils.getUsernameFromToken(jwtToken);
            User user = userDAO.findByUsername(username);  //  Obtener usuario para actualizar
            //  Codificar la clave segun el algoritmo establecido en la configuracion
            String passwordHashed = passwordEncoder.encode(password);
            user.setPassword(passwordHashed);
            //  Guardar usuario y retornar nuevo usuario
            return userDAO.save(user);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

}