package ar.edu.iua.trabajoIntegradorSOA2020.web.services;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ITankStatusBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;

import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatusOriginData;
import io.swagger.annotations.*;
import org.springframework.core.MethodParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(RESTConstants.URL_TANKS)
@Api(tags = "Tanks")
public class TankStatusRESTController {

    @Autowired
    private ITankStatusBusiness tankStatusBusiness;

    @ApiOperation(value = "Get last status of all tanks", authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_NOT_FOUND, message = "Not Found"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/estado", produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<List<TankStatus>> getAllTanksStatus(HttpServletRequest request) {
        try {
            return ResponseEntity.ok(tankStatusBusiness.getAllLast());
        } catch (NotFoundException e) {
            return new CustomizedResponseEntityExceptionHandler().handleNotFoundException(e, request);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }


    @ApiOperation(value = "Get last status of tank by id", response = TankStatus.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_NOT_FOUND, message = "Not Found"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/{id}/estado", produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<TankStatus> getTankStatus(@PathVariable("id") int id, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(tankStatusBusiness.getLast(id));
        } catch (NotFoundException e) {
            return new CustomizedResponseEntityExceptionHandler().handleNotFoundException(e, request);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }


    @ApiOperation(value = "Create a new tank status by specifying the id of the tank", response = TankStatus.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_CREATED, message = "Created"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_BAD_REQUEST, message = "Bad Request"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(value = "/{id}/medicion", consumes = RESTConstants.CONTENT_TYPE_JSON, produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<TankStatus> sendTankMeasurement(@PathVariable("id") int idTank, @RequestBody @Valid TankStatusOriginData tankStatusOriginData, UriComponentsBuilder uriComponentsBuilder, HttpServletRequest request, BindingResult bindingResult) {
        try {
            //  Revisar que los datos de estado tanque se hayan pasado bien
            if (bindingResult.hasErrors()) {
                throw new MethodArgumentNotValidException(
                    new MethodParameter(this.getClass().getDeclaredMethod("saveUser", int.class, TankStatus.class, UriComponentsBuilder.class, HttpServletRequest.class, BindingResult.class), 0),
                    bindingResult);
            }

            TankStatus tankStatusCreated = tankStatusBusiness.save(idTank, tankStatusOriginData);

            URI locationURI = uriComponentsBuilder
                    .path(RESTConstants.URL_TANKS + "/" + idTank + "/estado")
                    .buildAndExpand(tankStatusCreated.getIdTankStatus())
                    .toUri();

            return ResponseEntity.created(locationURI).body(tankStatusCreated);
        } catch (NotFoundException e) {
            return new CustomizedResponseEntityExceptionHandler().handleNotFoundException(e, request);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        } catch (MethodArgumentNotValidException e) {
            return new CustomizedResponseEntityExceptionHandler().handleMethodArgumentNotValidException(e, request);
        } catch (NoSuchMethodException | SecurityException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }
}