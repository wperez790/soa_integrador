package ar.edu.iua.trabajoIntegradorSOA2020.security;

import ar.edu.iua.trabajoIntegradorSOA2020.business.impl.services.MyUserDetailsService;
import ar.edu.iua.trabajoIntegradorSOA2020.model.ErrorDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

//  Clase que revisa el JWT de cada request
public class JWTFilter extends OncePerRequestFilter {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private JWTUtils jwtUtils;

    private MyUserDetailsService userDetailsService;

    public JWTFilter(MyUserDetailsService userDetailsService) {
        /*  No se usa Autowired porque el filtro no puede ser un Bean.
            La razón para que no sea un Bean es que si lo fuera, Spring lo 
            insertaría en la cadena de filtros por defecto, y por ende no se
            puede ignorar inclusive usando web.ignoring() para ciertas URLs.
            Ver: https://stackoverflow.com/questions/39152803/spring-websecurity-ignoring-doesnt-ignore-custom-filter,
            https://github.com/spring-projects/spring-security/issues/3958
        */  
        this.jwtUtils = new JWTUtils();
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
    throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader(SecurityConstants.HEADER_AUTHORIZATION);
        String username = null;
        String jwtToken = null;
        String errorMessage = "";
        // JWT Token se espera en la forma "Bearer " + token
        log.error("HEADER: {}", requestTokenHeader);
        if (requestTokenHeader != null && requestTokenHeader.startsWith(SecurityConstants.BEARER_TOKEN_PREFIX + " ")) {
            jwtToken = requestTokenHeader.replace(SecurityConstants.BEARER_TOKEN_PREFIX + " ", "");
            try {
                log.error("TOKEN: {}", jwtToken);
                username = jwtUtils.getUsernameFromToken(jwtToken);
                log.error("username: {}", username);
            } catch (MalformedJwtException e) {
                errorMessage = "JWT Token is malformed or is empty";
                log.error(errorMessage);
                setErrorResponse(request, response, errorMessage);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } catch (IllegalArgumentException e) {
                errorMessage = "Unable to get user from JWT Token.";
                log.error(errorMessage);
                setErrorResponse(request, response, errorMessage);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } catch (ExpiredJwtException e) {
                errorMessage = "JWT Token has expired";
                log.error(errorMessage);
                setErrorResponse(request, response, errorMessage);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        } else {
            errorMessage = "Unable to find token." +
            "Make sure it is present in Authorization header, following the Bearer schema.";
            log.error("JWT Token does not begin with Bearer String or is not present in Authorization Header.");
            setErrorResponse(request, response, errorMessage);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        // Validar Token
        if (username != null) {
            /*  Revisamos si no se han guardado ya datos de autenticacion en el hilo  */
            if (SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                //  Si un token es valido Configurar Spring Security
                //  para setear la autenticacion
                if (jwtUtils.validateToken(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, userDetails.getPassword(), userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    //  Luego de haber colocado la Autenticacion en el contexto, especificamos
                    //  que el usuario actual esta autenticado, para que pase la
                    //  configuracion de Spring Security
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                } else {
                    errorMessage = "JWT is not valid. Either token has expired or user hasn't been found on database.";
                    log.error(errorMessage);
                    setErrorResponse(request, response, errorMessage);
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    return;
                }
            }
        } else {
            log.error("Username got from token is null.");
            errorMessage = "Username not found at token \'sub\' claim.";
            setErrorResponse(request, response, errorMessage);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        chain.doFilter(request, response);
    }

    //  Funcion para insertar mensaje de error en el cuerpo de la respuesta
    private void setErrorResponse(HttpServletRequest request,
                                  HttpServletResponse response, String errorMessage) throws IllegalStateException, IOException {
        try {
            ErrorDetails errorDetails = new ErrorDetails(new Date(), errorMessage,
                request.getServletPath());
            //  Convert to json
            ObjectMapper mapper = new ObjectMapper();
            //  Configurar mapper para no serializar fechas como timestamp
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            String jsonMsg = mapper.writeValueAsString(errorDetails);
            response.setContentType("application/json");
            response.getOutputStream().println(jsonMsg);
        } catch(IllegalStateException | IOException ex) {
            log.error("Error while trying to generate Error Response Message for JWT filter.");
            throw ex;
        }
    }
}