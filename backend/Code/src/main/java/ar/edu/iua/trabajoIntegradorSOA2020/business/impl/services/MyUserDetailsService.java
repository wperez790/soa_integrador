package ar.edu.iua.trabajoIntegradorSOA2020.business.impl.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ar.edu.iua.trabajoIntegradorSOA2020.model.Role;
import ar.edu.iua.trabajoIntegradorSOA2020.model.User;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.UserRepository;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Component
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //  Obtener usuario de la bd
        User user = userDAO.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with username = " + username + " not found");
        }
        return convertToUserDetails(user);
    }

    private UserDetails convertToUserDetails(User user) {
        //  Pasar roles a GrantedAuthority
        List<Role> roles = user.getRoles();
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getRole().trim()));
        }
        //  Usamos el objeto ofrecido por SpringSecurity
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
            true, true, true, true, authorities);
    }
    
}