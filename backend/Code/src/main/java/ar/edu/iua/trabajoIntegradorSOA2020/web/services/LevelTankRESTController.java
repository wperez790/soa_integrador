package ar.edu.iua.trabajoIntegradorSOA2020.web.services;

import java.io.IOException;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ILevelTankBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TableOfTankUploadedDetails;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(RESTConstants.URL_TANKS)
@Api(tags = "Tanks")
public class LevelTankRESTController {

    @Autowired
    private ILevelTankBusiness levelTankBusiness;

    @ApiOperation(value = "Add the level table of tank by id", response = TableOfTankUploadedDetails.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_NOT_FOUND, message = "Not Found"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value = "/{id}/tablaNivel", consumes= RESTConstants.CONTENT_TYPE_FORM_DATA, produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<TableOfTankUploadedDetails> uploadMultipart(@RequestParam("file") MultipartFile file, @PathVariable("id") int idTank, HttpServletRequest request) throws IOException {
        try {
            String message = "Level table of tank was uploaded correctly.";
            int rowsInserted = levelTankBusiness.save(file.getInputStream(), idTank);

            TableOfTankUploadedDetails tableOfTankUploadedDetails = new TableOfTankUploadedDetails(message, rowsInserted);

            return ResponseEntity.ok(tableOfTankUploadedDetails);
        } catch (NotFoundException e) {
            return new CustomizedResponseEntityExceptionHandler().handleNotFoundException(e, request);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }

}