package ar.edu.iua.trabajoIntegradorSOA2020.business.impl;

import java.util.Optional;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ITankBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.utils.TankUtils;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.LevelTankRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;

@Service
public class TankBusiness implements ITankBusiness {

    private static Logger log = LoggerFactory.getLogger(TankBusiness.class);
    
    @Autowired
    private TankRepository tankDAO;

    @Autowired
    private LevelTankRepository levelTankDao;

    @Override
    public Tank getOne(int id) throws BusinessException, NotFoundException {
        log.info("Info when getting one Tank by id: Starting method logs.");
        Optional<Tank> tankFound;

        try {
            tankFound = tankDAO.findById(id);

            log.info("Info when getting one Tank by id: The Tank with id = {} was found.", id);
        } catch (Exception e) {
            log.error("Error when getting one Tank by id: {}.", e.getMessage());
            log.info("Info when getting one Tank by id: Finished method logs.");
            throw new BusinessException(e);
        }

        if (!tankFound.isPresent()) {
            log.error("Error when getting one Tank by id: The Tank with id = {} was not found.", id);
            log.info("Info when getting one Tank by id: Finished method logs.");
            throw new NotFoundException("The tank with id = " + id + " was not found.");
        }

        log.info("Info when getting one Tank by id: The Tank with id = {} was returned succesfully.", id);
        log.info("Info when getting one Tank by id: Finished method logs.");

        return tankFound.get();
    }

    @Override
    public Tank addTank(Tank tank) throws BusinessException {
        log.info("Info when adding one Tank: Starting method logs.");

        if (tank.getTll() == 0f ||
            tank.getMaxTemp() == null ||
            tank.getMaxTemp().floatValue() == 0f ||
            tank.getLevelAlarmStatus() == null ||
            tank.getLevelAlarmStatus().getAA() == 0f ||
            tank.getLevelAlarmStatus().getA() == 0f ||
            tank.getLevelAlarmStatus().getB() == 0f ||
            tank.getLevelAlarmStatus().getBB() == 0f) {

            log.error("Error when adding one Tank: tll, maxTemp, and the different level alarnm status limits fields must be entered.");
            log.info("Info when adding one Tank: Finished method logs.");
            throw new BusinessException("tll, maxTemp, and the different level alarnm status limits fields must be entered.");
        }

        try {
            Tank tankSaved = tankDAO.save(tank);

            log.info("Info when adding one Tank: The Tank with id = {} was saved succesfully.", tankSaved.getIdTank());
            return tankSaved;
        } catch (Exception e) {
            log.error("Error when adding one Tank: {}.", e.getMessage());
            throw new BusinessException(e);
        } finally {
            log.info("Info when adding one Tank: Finished method logs.");
        }
    }

    @Override
    public Tank modifyTank(int id, Tank tank) throws BusinessException {
        log.info("Info when modifying one Tank: Starting method logs.");

        if (tank.getTll() == 0f &&
            tank.getMaxTemp() == null ||
            tank.getMaxTemp().floatValue() == 0f ||
            tank.getLevelAlarmStatus() == null) {

            log.error("Error when modifying one Tank: One of the tll, maxTemp, and the different level alarm status limits fields must be entered.");
            log.info("Info when modifying one Tank: Finished method logs.");
            throw new BusinessException("One of the tll, maxTemp, and the different level alarm status limits fields must be entered.");
        }

        Tank tankFound = getOne(id);

        if (tank.getTll() != 0f) {
            int vol = levelTankDao.levelToVol(tank.getTll(), id);
            tank.setTlv(vol);
        }

        Tank tankModified = TankUtils.updateTankObject(tankFound, tank);

        try {
            Tank tankSaved = tankDAO.save(tankModified);
            log.info("Info when adding one Tank: The Tank with id = {} was saved succesfully.", tankSaved.getIdTank());
            return tankSaved;
        } catch (Exception e) {
            log.error("Error when modifying one Tank: {}.", e.getMessage());
            throw new BusinessException(e);
        } finally {
            log.info("Info when modifying one Tank: Finished method logs.");
        }
    }

}