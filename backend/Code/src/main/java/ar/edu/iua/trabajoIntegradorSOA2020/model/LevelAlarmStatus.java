package ar.edu.iua.trabajoIntegradorSOA2020.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="level_alarm_status")
public class LevelAlarmStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int idLevelAlarmStatus;

    @NotNull(message = "AA cannot be null")
    @ApiModelProperty(notes = "number representing the Level Alarm Status 'Alto Alto'", required = true)
    private float aa = 0f;

    @NotNull(message = "A cannot be null")
    @ApiModelProperty(notes = "number representing the Level Alarm Status 'Alto'", required = true)
    private float a = 0f;

    @NotNull(message = "B cannot be null")
    @ApiModelProperty(notes = "number representing the Level Alarm Status 'Bajo'", required = true)
    private float b = 0f;

    @NotNull(message = "BB cannot be null")
    @ApiModelProperty(notes = "number representing the Level Alarm Status 'Bajo Bajo'", required = true)
    private float bb = 0f;

    @OneToOne(mappedBy = "levelAlarmStatus")
    @JsonIgnore
    private Tank tank;

    public int getIdLevelAlarmStatus() {
        return idLevelAlarmStatus;
    }

    public void setIdLevelAlarmStatus(int idLevelAlarmStatus) {
        this.idLevelAlarmStatus = idLevelAlarmStatus;
    }

    public float getAA() {
        return aa;
    }

    public void setAA(float aa) {
        this.aa = aa;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getBB() {
        return bb;
    }

    public void setBB(float bb) {
        this.bb = bb;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
}