package ar.edu.iua.trabajoIntegradorSOA2020.business.utils;

import ar.edu.iua.trabajoIntegradorSOA2020.model.LevelAlarmStatus;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;

import java.util.HashMap;
import java.util.Map;

class TankLevelAlarmStatusUtils {
    private static Map<String, String> levelAlarmStatusMap = new HashMap<>();

    private static void initLevelAlarmStatusMap() {
        levelAlarmStatusMap.put("AA", "aa");
        levelAlarmStatusMap.put("A", "a");
        levelAlarmStatusMap.put("NORMAL", "-");
        levelAlarmStatusMap.put("B", "b");
        levelAlarmStatusMap.put("BB", "bb");
    }

    static String setLevelAlarmStatus(TankStatus tankStatus) {
        String levelAlarmStatus;

        initLevelAlarmStatusMap();

        LevelAlarmStatus las = tankStatus.getTank().getLevelAlarmStatus();

        if (tankStatus.getLevel() > las.getAA()) {
            levelAlarmStatus = levelAlarmStatusMap.get("AA");
        } else if (tankStatus.getLevel() <= las.getAA() && tankStatus.getLevel() > las.getA()) {
            levelAlarmStatus = levelAlarmStatusMap.get("A");
        } else if (tankStatus.getLevel() <= las.getA() && tankStatus.getLevel() > las.getB()) {
            levelAlarmStatus = levelAlarmStatusMap.get("NORMAL");
        } else if (tankStatus.getLevel() <= las.getB() && tankStatus.getLevel() > las.getBB()) {
            levelAlarmStatus = levelAlarmStatusMap.get("B");
        } else {
            levelAlarmStatus = levelAlarmStatusMap.get("BB");
        }

        return levelAlarmStatus;
    }
}
