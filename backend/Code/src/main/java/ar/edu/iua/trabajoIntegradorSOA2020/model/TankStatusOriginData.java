package ar.edu.iua.trabajoIntegradorSOA2020.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

public class TankStatusOriginData {

    @ApiModelProperty(notes = "level measured in the tank", required = true)
    @NotNull(message = "level cannot be null")
    private float level;

    @ApiModelProperty(notes = "temperature measured in the tank", required = true)
    @Column(precision = 4, scale = 1)
    @NotNull(message = "temperature cannot be null")
    private BigDecimal temperature;

    @ApiModelProperty(notes = "density measured in the tank", required = true)
    @Column(precision = 4, scale = 3)
    @NotNull(message = "density cannot be null")
    private BigDecimal density;

    @ApiModelProperty(notes = "water Level measured in the tank", required = true)
    @NotNull(message = "water Level cannot be null")
    private float waterLevel;

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public BigDecimal getDensity() {
        return density;
    }

    public void setDensity(BigDecimal density) {
        this.density = density;
    }

    public float getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(float waterLevel) {
        this.waterLevel = waterLevel;
    }

}