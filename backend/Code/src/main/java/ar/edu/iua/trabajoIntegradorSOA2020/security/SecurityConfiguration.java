package ar.edu.iua.trabajoIntegradorSOA2020.security;

import ar.edu.iua.trabajoIntegradorSOA2020.web.services.RESTConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import ar.edu.iua.trabajoIntegradorSOA2020.business.impl.services.MyUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Value("${recursos.estaticos}")
    private String recursosEstaticos;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(myUserDetailsService).passwordEncoder(hashPassword());
    }

    @Bean
    public PasswordEncoder hashPassword() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        //  Evitar que estos recursos pasen por el filtro JWT
        String[] resources = recursosEstaticos.split(",");
        web
        .ignoring()
                //  Desactivar Filtros para GET /login
                .antMatchers(HttpMethod.GET, resources);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        LoginFilter loginFilter = new LoginFilter(authenticationManager());
        loginFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(RESTConstants.URL_LOGIN));

        //  Desactivamos para simplificar interacciones
        //  con el controlador, puesto que ya usamos JWT
        httpSecurity.csrf().disable();

        httpSecurity
            // Permitir a todos loguearse
            .authorizeRequests()
            //  Limitar acceso a /usuarios
            .antMatchers(RESTConstants.URL_USERS).hasAuthority("ADMIN")
            //  Limitar acceso a /tanques
            .antMatchers(RESTConstants.URL_TANKS + "/**").hasAnyAuthority("ADMIN", "CONTROLADOR")
            .and()
            //  FormLogin
            .formLogin()
                .loginPage(RESTConstants.URL_LOGIN)
            .and()
            // Modo Stateless
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            //  Pedir HTTPS
            .requiresChannel().anyRequest().requiresSecure()
            .and()
            //  Agregar filtros para login y token
            .addFilterBefore(loginFilter,
            UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(new JWTFilter(myUserDetailsService),
            UsernamePasswordAuthenticationFilter.class);
        }
        
    }