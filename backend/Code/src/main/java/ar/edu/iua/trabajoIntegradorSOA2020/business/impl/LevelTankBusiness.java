package ar.edu.iua.trabajoIntegradorSOA2020.business.impl;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ILevelTankBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.utils.CSVUtils;
import ar.edu.iua.trabajoIntegradorSOA2020.model.LevelTank;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.LevelTankRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class LevelTankBusiness implements ILevelTankBusiness {

    private static Logger log = LoggerFactory.getLogger(LevelTankBusiness.class);

    @Autowired
    private LevelTankRepository levelTankDAO;

    @Autowired
    private TankRepository tankDAO;

    @Override
    public int save(InputStream levelTankCsv, int id) throws BusinessException, NotFoundException {
        log.info("Info when adding the tank's level table: Starting method logs.");

        int rowsInserted;
        try {
            List<LevelTank> levelTankList = CSVUtils.read(LevelTank.class, levelTankCsv);
            Optional<Tank> op = tankDAO.findById(id);

            if (!op.isPresent()) {
                log.error("Error when adding the tank's level table: No se encuentra el tanque con id = {}", id);
                log.info("Info when adding the tank's level table: Finished method logs.");
                throw new NotFoundException("The tank with the id = " + id + " was not found.");
            }

            for (LevelTank levelTank: levelTankList) {
                levelTank.setTank(op.get());
                levelTankDAO.save(levelTank);
            }

            rowsInserted = levelTankList.size();
            log.info("Info when adding the tank's level table: The number of rows inserted succesfully in the table are = {}.", rowsInserted);

            if (rowsInserted > 0) {
                float tlv = levelTankDAO.levelToVol(op.get().getTll(), op.get().getIdTank());
                tankDAO.updateTankTlv(tlv, op.get().getIdTank());
                log.info("Info when adding the tank's level table: The tank with id = {} updated it's tlv successfully.", id);
            }
        } catch (Exception e) {
            log.error("Error when adding the tank's level table: {}.", e.getMessage());
            throw new BusinessException(e);
        }
        log.info("Error when adding the tank's level table: Finished method logs.");
        return rowsInserted;
    }

}