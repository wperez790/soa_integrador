package ar.edu.iua.trabajoIntegradorSOA2020.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "tank")
public class Tank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "id of the tank")
    private int idTank;

    @ApiModelProperty(notes = "total load level of the tank", required = true)
    @NotNull(message = "tll cannot be null")
    private float tll = 0f;

    @ApiModelProperty(notes = "total load volume of the tank", required = true)
    @NotNull(message = "tlv cannot be null")
    private float tlv = 0f;

    @ApiModelProperty(notes = "maximum temperature allowed in the tank", required = true)
    @Column(precision = 4, scale = 1)
    @NotNull(message = "maximum temperature cannot be null")
    private BigDecimal maxTemp;

    @OneToMany(mappedBy = "tank", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<TankStatus> tankStatusList;

    @OneToMany(mappedBy = "tank", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<LevelTank> levelTankList;

    @OneToMany(mappedBy = "tank", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Vcf> vcfList;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tanque_idlevel_alarm_status", referencedColumnName = "idLevelAlarmStatus")
    @NotNull
    private LevelAlarmStatus levelAlarmStatus;

    public int getIdTank() {
        return idTank;
    }

    public void setIdTank(int idTank) {
        this.idTank = idTank;
    }

    public float getTll() {
        return tll;
    }

    public void setTll(float tll) {
        this.tll = tll;
    }

    public float getTlv() {
        return tlv;
    }

    public void setTlv(float tlv) {
        this.tlv = tlv;
    }

    public BigDecimal getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(BigDecimal maxTemp) {
        this.maxTemp = maxTemp;
    }

    public List<TankStatus> getTankStatusList() {
        return tankStatusList;
    }

    public void setTankStatusList(List<TankStatus> tankStatusList) {
        this.tankStatusList = tankStatusList;
    }

    public List<LevelTank> getLevelTankList() {
        return levelTankList;
    }

    public void setLevelTankList(List<LevelTank> levelTankList) {
        this.levelTankList = levelTankList;
    }

    @JsonIgnore
    public List<Vcf> getTankVcfList() {
        return vcfList;
    }

    public void setTankVcfList(List<Vcf> vcfList) {
        this.vcfList = vcfList;
    }

    public LevelAlarmStatus getLevelAlarmStatus() {
        return levelAlarmStatus;
    }

    public void setLevelAlarmStatus(LevelAlarmStatus levelAlarmStatus) {
        this.levelAlarmStatus = levelAlarmStatus;
    }
}