package ar.edu.iua.trabajoIntegradorSOA2020.business.utils;

import ar.edu.iua.trabajoIntegradorSOA2020.model.LevelAlarmStatus;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;

public class TankUtils {
    public static Tank updateTankObject(Tank originalTank, Tank changedTank) {
        Tank modifiedTank = new Tank();
        modifiedTank.setIdTank(originalTank.getIdTank());

        if (changedTank.getTll() != 0f) {
            modifiedTank.setTll(changedTank.getTll());
        } else {
            modifiedTank.setTll(originalTank.getTll());
        }

        if (changedTank.getTlv() != 0f) {
            modifiedTank.setTlv(changedTank.getTlv());
        } else {
            modifiedTank.setTlv(originalTank.getTlv());
        }

        if (changedTank.getMaxTemp() != null && changedTank.getMaxTemp().floatValue() != 0f) {
            modifiedTank.setMaxTemp(changedTank.getMaxTemp());
        } else {
            modifiedTank.setMaxTemp(originalTank.getMaxTemp());
        }

        if (changedTank.getLevelAlarmStatus() != null) {
            LevelAlarmStatus changedTankLAS = changedTank.getLevelAlarmStatus();
            LevelAlarmStatus modifiedTankLAS = new LevelAlarmStatus();

            if (changedTankLAS.getAA() != 0f) {
                modifiedTankLAS.setAA(changedTankLAS.getAA());
            } else {
                modifiedTankLAS.setAA(originalTank.getLevelAlarmStatus().getAA());
            }

            if (changedTankLAS.getA() != 0f) {
                modifiedTankLAS.setA(changedTankLAS.getA());
            } else {
                modifiedTankLAS.setA(originalTank.getLevelAlarmStatus().getA());
            }

            if (changedTankLAS.getB() != 0f) {
                modifiedTankLAS.setB(changedTankLAS.getB());
            } else {
                modifiedTankLAS.setB(originalTank.getLevelAlarmStatus().getB());
            }

            if (changedTankLAS.getBB() != 0f) {
                modifiedTankLAS.setBB(changedTankLAS.getBB());
            } else {
                modifiedTankLAS.setBB(originalTank.getLevelAlarmStatus().getBB());
            }

            modifiedTank.setLevelAlarmStatus(modifiedTankLAS);
        } else {
            modifiedTank.setLevelAlarmStatus(originalTank.getLevelAlarmStatus());
        }

        return modifiedTank;
    }
}