package ar.edu.iua.trabajoIntegradorSOA2020.model;

import javax.persistence.*;

import java.math.BigDecimal;

@Entity
@Table(name = "vcf")
public class Vcf {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idVcf;

    @Column(precision = 4, scale = 3)
    private BigDecimal density;

    @Column(precision = 4, scale = 1)
    private BigDecimal temperature;

    @Column(precision = 5, scale = 4)
    private BigDecimal vcf;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tank_id", referencedColumnName = "idtank")
    private Tank tank;

    public int getIdVcf() {
        return idVcf;
    }

    public void setIdVcf(int idVcf) {
        this.idVcf = idVcf;
    }

    public BigDecimal getDensity() {
        return density;
    }

    public void setDensity(BigDecimal density) {
        this.density = density;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public BigDecimal getVcf() {
        return vcf;
    }

    public void setVcf(BigDecimal vcf) {
        this.vcf = vcf;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
}