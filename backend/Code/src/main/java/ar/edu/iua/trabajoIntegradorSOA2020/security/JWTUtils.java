package ar.edu.iua.trabajoIntegradorSOA2020.security;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import ar.edu.iua.trabajoIntegradorSOA2020.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtils implements Serializable {
    private static final long serialVersionUID = -2550185165626007488L;

    // Obtener nombre de usuario
    public String getUsernameFromToken(String token) {
        return getTokenClaim(token, Claims::getSubject);
    }

    //  Obtener fecha de expiracion
    private Date getExpirationDateFromToken(String token) {
        return getTokenClaim(token, Claims::getExpiration);
    }

    private <T> T getTokenClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getClaims(token);
        return claimsResolver.apply(claims);
    }

    //  Obtener todos los claims del JWT
    private Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(SecurityConstants.SECRET_SIGNATURE).parseClaimsJws(token).getBody();
    }

    //  Revisar si Token expiro
    private Boolean isTokenExpired(String token) {
        final Date exp = getExpirationDateFromToken(token);
        return exp.before(new Date());
    }

    //  Generar Token para usuario
    String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        String subject = user.getUsername();
        //  Crear el Token
        //  Compactar de acuerdo con (https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.TOKEN_LIFE_TIME))
            .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET_SIGNATURE).compact();
    }

    // Validar Token.
    Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}