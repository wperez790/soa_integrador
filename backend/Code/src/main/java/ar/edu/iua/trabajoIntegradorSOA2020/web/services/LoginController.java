package ar.edu.iua.trabajoIntegradorSOA2020.web.services;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(RESTConstants.URL_LOGIN)
public class LoginController {

    @GetMapping(value = {"", "/"})
    public String getLogin(){
        return "index";
    }
}