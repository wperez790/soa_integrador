package ar.edu.iua.trabajoIntegradorSOA2020.persistence;

import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TankStatusRepository extends JpaRepository<TankStatus, Integer> {
    @Query(value = "SELECT * FROM tank_status WHERE tank_id = ?1 ORDER BY datetime DESC LIMIT 1", nativeQuery = true)
    Optional<TankStatus> searchLastTankStatusById(int id);
}