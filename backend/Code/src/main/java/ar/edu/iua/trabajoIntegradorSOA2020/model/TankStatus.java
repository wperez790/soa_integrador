package ar.edu.iua.trabajoIntegradorSOA2020.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.math.BigDecimal;

@Entity
@Table(name="tank_status")
public class TankStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "id of the tank status")
    private int idTankStatus;

    @ApiModelProperty(notes = "date and time when the tank status was generated")
    private String datetime;

    @ApiModelProperty(notes = "level measured in the tank", required = true)
    @NotNull(message = "level cannot be null")
    private float level;

    @ApiModelProperty(notes = "temperature measured in the tank", required = true)
    @Column(precision = 4, scale = 1)
    @NotNull(message = "temperature cannot be null")
    private BigDecimal temperature;

    @ApiModelProperty(notes = "density measured in the tank", required = true)
    @Column(precision = 4, scale = 3)
    @NotNull(message = "density cannot be null")
    private BigDecimal density;

    @ApiModelProperty(notes = "water level measured in the tank", required = true)
    @NotNull(message = "water level cannot be null")
    private float waterLevel;

    @ApiModelProperty(notes = "product level in the tank")
    private float productLevel;

    @ApiModelProperty(notes = "total observed volume in the tank")
    private float tov;

    @ApiModelProperty(notes = "water volume in the tank")
    private float waterVol;

    @ApiModelProperty(notes = "gross observed volume in the tank")
    private float gov;

    @ApiModelProperty(notes = "total load level of the tank")
    @Transient
    private float tll;

    @ApiModelProperty(notes = "total load volume of the tank")
    @Transient
    private float tlv;

    @ApiModelProperty(notes = "load percent of the tank")
    private float loadPercent;

    @ApiModelProperty(notes = "available room in the tank")
    private float availableRoom;

    @ApiModelProperty(notes = "gross standard volume of the tank")
    @Column(precision = 9, scale = 4)
    private BigDecimal gsv;

    @ApiModelProperty(notes = "string representing the tank's level alarm status")
    private String levelAlarmStatus;

    @ApiModelProperty(notes = "boolean representing if the maximum tank temperature was reached")
    private boolean temperatureAlarm;

    @ApiModelProperty(notes = "difference between the new and the last Product Level's of the tank")
    private float deltaProductLevel;

    @ApiModelProperty(notes = "difference between the new and the last Temperature's measured in the tank")
    @Column(precision = 4, scale = 1)
    private BigDecimal deltaTemperature;

    @ApiModelProperty(notes = "difference between the new and the last Water Level's measured in the tank")
    private float deltaWaterLevel;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tank_id", referencedColumnName = "idTank")
    @JsonIgnore
    private Tank tank;
    
    @ApiModelProperty(notes = "id of the tank")
    @Transient
    private int idTank;

    public int getIdTankStatus() {
        return idTankStatus;
    }

    public void setIdTankStatus(int idTankStatus) {
        this.idTankStatus = idTankStatus;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public BigDecimal getDensity() {
        return density;
    }

    public void setDensity(BigDecimal density) {
        this.density = density;
    }

    public float getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(float waterLevel) {
        this.waterLevel = waterLevel;
    }

    public float getProductLevel() {
        return productLevel;
    }

    public void setProductLevel(float productLevel) {
        this.productLevel = productLevel;
    }

    public float getTov() {
        return tov;
    }

    public void setTov(float tov) {
        this.tov = tov;
    }

    public float getWaterVol() {
        return waterVol;
    }

    public void setWaterVol(float waterVol) {
        this.waterVol = waterVol;
    }

    public float getGov() {
        return gov;
    }

    public void setGov(float gov) {
        this.gov = gov;
    }

    public float getTll() {
        return tll;
    }

    public void setTll(float tll) {
        this.tll = tll;
    }

    public float getTlv() {
        return tlv;
    }

    public void setTlv(float tlv) {
        this.tlv = tlv;
    }

    public float getLoadPercent() {
        return loadPercent;
    }

    public void setLoadPercent(float loadPercent) {
        this.loadPercent = loadPercent;
    }

    public float getAvailableRoom() {
        return availableRoom;
    }

    public void setAvailableRoom(float availableRoom) {
        this.availableRoom = availableRoom;
    }

    public BigDecimal getGsv() {
        return gsv;
    }

    public void setGsv(BigDecimal gsv) {
        this.gsv = gsv;
    }

    public String getLevelAlarmStatus() {
        return levelAlarmStatus;
    }

    public void setLevelAlarmStatus(String levelAlarmStatus) {
        this.levelAlarmStatus = levelAlarmStatus;
    }

    public boolean isTemperatureAlarm() {
        return temperatureAlarm;
    }

    public void setTemperatureAlarm(boolean temperatureAlarm) {
        this.temperatureAlarm = temperatureAlarm;
    }

    public float getDeltaProductLevel() {
        return deltaProductLevel;
    }

    public void setDeltaProductLevel(float deltaProductLevel) {
        this.deltaProductLevel = deltaProductLevel;
    }

    public BigDecimal getDeltaTemperature() {
        return deltaTemperature;
    }

    public void setDeltaTemperature(BigDecimal deltaTemperature) {
        this.deltaTemperature = deltaTemperature;
    }

    public float getDeltaWaterLevel() {
        return deltaWaterLevel;
    }

    public void setDeltaWaterLevel(float deltaWaterLevel) {
        this.deltaWaterLevel = deltaWaterLevel;
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
    
    public int getIdTank() {
        return idTank;
    }

    public void setIdTank(int idTank) {
        this.idTank = idTank;
    }
    
    @Override
    public String toString() {
        return "TankStatus{" +
                "idTankStatus=" + idTankStatus +
                ", datetime='" + datetime + '\'' +
                ", level=" + level +
                ", temperature=" + temperature +
                ", density=" + density +
                ", waterLevel=" + waterLevel +
                ", productLevel=" + productLevel +
                ", tov=" + tov +
                ", waterVol=" + waterVol +
                ", gov=" + gov +
                ", tll=" + tll +
                ", tlv=" + tlv +
                ", loadPercent=" + loadPercent +
                ", availableRoom=" + availableRoom +
                ", gsv=" + gsv +
                ", levelAlarmStatus='" + levelAlarmStatus + '\'' +
                ", temperatureAlarm=" + temperatureAlarm +
                ", deltaProductLevel=" + deltaProductLevel +
                ", deltaTemperature=" + deltaTemperature +
                ", deltaWaterLevel=" + deltaWaterLevel +
                ", tank=" + tank +
                '}';
    }
}
