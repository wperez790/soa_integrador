package ar.edu.iua.trabajoIntegradorSOA2020.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.edu.iua.trabajoIntegradorSOA2020.model.Vcf;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;

public interface VcfRepository extends JpaRepository<Vcf, Integer> {
    @Query(value = "SELECT tanquesdb.calculateVcf(?1, ?2, ?3)", nativeQuery = true)
    BigDecimal calculateVcf(BigDecimal dens, BigDecimal temp, int tank_id);
}