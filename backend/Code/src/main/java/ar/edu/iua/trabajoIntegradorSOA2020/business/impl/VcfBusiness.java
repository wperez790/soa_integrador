package ar.edu.iua.trabajoIntegradorSOA2020.business.impl;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Optional;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankRepository;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.iua.trabajoIntegradorSOA2020.business.IVcfBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Vcf;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.VcfRepository;

@Service
public class VcfBusiness implements IVcfBusiness{

	private static Logger log = LoggerFactory.getLogger(VcfBusiness.class);

	@Autowired
	private VcfRepository vcfDAO;

	@Autowired
	private TankRepository tankDAO;

	@Override
	public int save(InputStream excelFile, int idTank) throws BusinessException, NotFoundException {

		int rowsInserted = 0;
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
			XSSFSheet worksheet = workbook.getSheetAt(0);

			Optional<Tank> op = tankDAO.findById(idTank);

			if (!op.isPresent()) {
				throw new NotFoundException("The tank with id = " + idTank + " was not found.");
			}

			XSSFRow densityRow = worksheet.getRow(0);

			Vcf vcf;
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				XSSFRow row = worksheet.getRow(i);

				for (int j = 1; j < row.getPhysicalNumberOfCells(); j++) {
					vcf = new Vcf();
					vcf.setTemperature(new BigDecimal(row.getCell(0).getNumericCellValue()));
					vcf.setDensity(new BigDecimal(densityRow.getCell(j).getNumericCellValue()));
					vcf.setVcf(new BigDecimal(row.getCell(j).getNumericCellValue()));
					vcf.setTank(op.get());

					vcfDAO.save(vcf);
					rowsInserted++;
					log.info("row #{}, col #{}, rowsInserted {}", i, j, rowsInserted);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}

		return rowsInserted;
	}

}