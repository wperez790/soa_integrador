package ar.edu.iua.trabajoIntegradorSOA2020.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private int id;

    @ApiModelProperty(notes = "username of the user", required = true)
    @NotNull(message = "username cannot be null")
    private String username;

    @ApiModelProperty(notes = "password of the user", required = true)
    @NotNull(message = "password cannot be null")
    private String password;

    @ApiModelProperty(notes = "roles of the user", required = true)
    @NotNull(message = "role list cannot be null")
    @ManyToMany(targetEntity = Role.class, fetch = FetchType.EAGER)
    private List<Role> roles;

    public User() {}
    
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public User(String username) {
        this.username = username;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}