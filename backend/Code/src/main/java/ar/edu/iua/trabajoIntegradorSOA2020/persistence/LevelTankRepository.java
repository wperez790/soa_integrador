package ar.edu.iua.trabajoIntegradorSOA2020.persistence;

import ar.edu.iua.trabajoIntegradorSOA2020.model.LevelTank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LevelTankRepository extends JpaRepository<LevelTank, Integer> {
    @Query(value = "SELECT tanquesdb.levelToVol(?1, ?2)", nativeQuery = true)
    Integer levelToVol(float level, int tank_id);
}