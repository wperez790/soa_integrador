package ar.edu.iua.trabajoIntegradorSOA2020.business;

import java.io.InputStream;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;

public interface ILevelTankBusiness {
    int save(InputStream levelTankCsv, int id) throws BusinessException, NotFoundException;
}
