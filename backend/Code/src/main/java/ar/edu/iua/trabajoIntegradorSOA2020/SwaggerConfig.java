package ar.edu.iua.trabajoIntegradorSOA2020;

import ar.edu.iua.trabajoIntegradorSOA2020.security.SecurityConstants;
import ar.edu.iua.trabajoIntegradorSOA2020.web.services.RESTConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;

@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket apiConfig() {
		return new Docket(DocumentationType.SWAGGER_2)
				.protocols(new HashSet<>(Collections.singletonList(RESTConstants.HTTPS_PROTOCOL)))
				.consumes(new HashSet<>(Collections.singletonList(RESTConstants.CONTENT_TYPE_JSON)))
				.produces(new HashSet<>(Collections.singletonList(RESTConstants.CONTENT_TYPE_JSON)))
				.useDefaultResponseMessages(false)
				.tags(
					new Tag("Tanks", "Operations related to tanks in the API"),
					new Tag("Users", "Operations related to users in the API")
				)
				.select()
				.apis(RequestHandlerSelectors.basePackage("ar.edu.iua.trabajoIntegradorSOA2020"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo())
				.securitySchemes(Collections.singletonList(apiKey()));
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
				"Tanks API",
				"Backend API that give access to the Tanks endpoints",
				"1.0.0",
				"",
				new Contact("Walter Perez, Agustin Pereyra, Ivan Guerra",
						"https://wperez790@bitbucket.org/wperez790/soa_integrador.git",
						"wperez790@alumnos.iua.edu.ar, apereyra404@alumnos.iua.edu.ar,  iguerra696@alumnos.iua.edu.ar"),
				"Apache License Version 2.0",
				"https://www.apache.org/licenses/LICENSE-2.0",
				Collections.emptyList()
				);
	}

	private ApiKey apiKey() {
		return new ApiKey("JWT Token", SecurityConstants.HEADER_AUTHORIZATION, "header");
	}

}