package ar.edu.iua.trabajoIntegradorSOA2020.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.edu.iua.trabajoIntegradorSOA2020.model.User;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LoginFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    //  No autowired por referencia ciclica con ConfiguracionSeguridad
    private JWTUtils jwtUtils;

    LoginFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        jwtUtils = new JWTUtils();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            User credentials = new ObjectMapper().readValue(request.getInputStream(), User.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    credentials.getUsername(), credentials.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException {
        //  Usamos el nombre del Authentication
        User user = new User(auth.getName());
        String token = jwtUtils.generateToken(user);
        //  Crear JSON con JWT
        String json = "{ \"access_token\": \"" + token + "\"}";
        //  Escribir mensaje en bytes para compatibilidad con
        //  org.apache.catalina.connector.Response.getOutputStream(Response.java:549)
        //  que escribe otras cosas que se agreguen luego al response con outputStream.
        //  Y response solo acepta que se escriba con outputstream o getWriter
        response.setContentLength(json.length());    //  Es necesario actualizar la cabecera
        ServletOutputStream body = response.getOutputStream();
        byte[] bytes = json.getBytes();
        body.write(bytes);
        //  Colocar en Header para pasar el FiltroJWT
        response.addHeader(SecurityConstants.HEADER_AUTHORIZATION, SecurityConstants.BEARER_TOKEN_PREFIX + " " + token);
    }
}