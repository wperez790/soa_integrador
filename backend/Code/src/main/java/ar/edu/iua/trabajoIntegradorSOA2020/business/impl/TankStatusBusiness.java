package ar.edu.iua.trabajoIntegradorSOA2020.business.impl;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ITankStatusBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.converter.JSONStringToTankStatusConverter;
import ar.edu.iua.trabajoIntegradorSOA2020.business.converter.TankStatusToJSONStringConverter;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.utils.TankStatusUtils;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatusOriginData;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.LevelTankRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankStatusRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.VcfRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TankStatusBusiness implements ITankStatusBusiness {

    private static Logger log = LoggerFactory.getLogger(TankStatusBusiness.class);

    private static final String REDIS_KEY = "/estado/";

    @Autowired
    private TankStatusRepository tankStatusDao;

    @Autowired
    private LevelTankRepository levelTankDao;

    @Autowired
    private VcfRepository vcfDao;

    @Autowired
    private TankRepository tankDao;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private TankStatusToJSONStringConverter TSToJSONStringConverter;

    @Autowired
    private JSONStringToTankStatusConverter JSONStringToTSConverter;

    @Override
    public List<TankStatus> getAllLast() throws BusinessException, NotFoundException {
        log.info("Info when getting the last status of all the tanks: Starting method logs.");

        List<TankStatus> tankStatusList = new ArrayList<>();
        List<Integer> tankIdList = tankDao.getAllTanksId();

        TankStatus tsFound;

        for (Integer id : tankIdList) {
            tsFound = getLast(id);
            tankStatusList.add(tsFound);
        }

        log.info("Info when getting the last status of all the tanks: The status of all the tanks were found successfully.");
        log.info("Info when getting the last status of all the tanks: Finished method logs.");

        return tankStatusList;
    }

    @Override
    public TankStatus getLast(int id) throws BusinessException, NotFoundException {
        log.info("Info when getting the last status of the Tank by id: Starting method logs.");
        ValueOperations valueOperations = redisTemplate.opsForValue();
        TankStatus tsFound;
        String ts;

        if (!redisTemplate.hasKey(REDIS_KEY.concat(String.valueOf(id)))) {
            log.error("Error when getting the last status of the Tank by id in cache: The status of the Tank with id = {} was not found.", id);
            log.info("Info when getting the last status of the Tank by id in cache: Finished method logs.");

            Optional<TankStatus> tankStatus;

            try {
                tankStatus = tankStatusDao.searchLastTankStatusById(id);
            } catch (Exception e) {
//                log.error("Error when getting one Tank by id: {}.", e.getMessage());
//                log.info("Info when getting one Tank by id: Finished method logs.");
                throw new BusinessException(e);
            }

            if (!tankStatus.isPresent()) {
//                log.error("Error when getting one Tank by id: The Tank with id = {} was not found.", id);
//                log.info("Info when getting one Tank by id: Finished method logs.");
                throw new NotFoundException("The status of the Tank with id = " + id + " was not found.");
            }

            TankStatusOriginData originData = new TankStatusOriginData();
            originData.setLevel(tankStatus.get().getLevel());
            originData.setTemperature(tankStatus.get().getTemperature());
            originData.setDensity(tankStatus.get().getDensity());
            originData.setWaterLevel(tankStatus.get().getWaterLevel());

            save(id, originData);
        }

        try {
            ts = (String) valueOperations.get(REDIS_KEY.concat(String.valueOf(id)));

            log.info("Info when getting the last status of the Tank by id: The status of the Tank with id = {} was found.", id);
        } catch (Exception e) {
            log.error("Error when getting the last status of the Tank by id: {}.", e.getMessage());
            log.info("Info when getting the last status of the Tank by id: Finished method logs.");
            throw new BusinessException(e);
        }

        tsFound = JSONStringToTSConverter.convert(ts);
        assert tsFound != null;

        log.info("Info when getting the last status of the Tank by id: The status of the Tank with id = {} was found.", id);

        log.info("Info when getting the last status of the Tank by id: Finished method logs.");
        return tsFound;
    }

	@Override
	public TankStatus save(int idTank, TankStatusOriginData tankStatusOriginData) throws BusinessException, NotFoundException {
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setValueSerializer(new StringRedisSerializer());

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();

        TankStatus tankStatus = new TankStatus();

        tankStatus.setLevel(tankStatusOriginData.getLevel());
        tankStatus.setTemperature(tankStatusOriginData.getTemperature());
        tankStatus.setDensity(tankStatusOriginData.getDensity());
        tankStatus.setWaterLevel(tankStatusOriginData.getWaterLevel());
        tankStatus.setIdTank(idTank);

        try {
            Optional<Tank> tankFound = tankDao.findById(idTank);

            if (!tankFound.isPresent()) {
                throw new NotFoundException("The tank with the id = " + idTank + " was not found.");
            }

            tankStatus.setTank(tankFound.get());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            tankStatus.setDatetime(ZonedDateTime.now().format(formatter));

            // Completar demas campos
            TankStatus tankStatusComplete = TankStatusUtils.completeTankStatusAttributes(tankStatus, tankStatusDao, levelTankDao, vcfDao);

            TankStatus taskStatusSaved = tankStatusDao.save(tankStatusComplete);

            valueOperations.set(REDIS_KEY.concat(String.valueOf(idTank)), TSToJSONStringConverter.convert(taskStatusSaved));

            return taskStatusSaved;
        } catch(NotFoundException e) {
            throw e;
        } catch(Exception e) {
            throw new BusinessException(e);
        }
	}

}