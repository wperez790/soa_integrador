package ar.edu.iua.trabajoIntegradorSOA2020.model;


import io.swagger.annotations.ApiModelProperty;

public class TableOfTankUploadedDetails {

    @ApiModelProperty(notes = "result message")
    private String message;

    @ApiModelProperty(notes = "number of rows uploaded")
    private int rowsUploaded;

    public TableOfTankUploadedDetails(String message, int rowsUploaded) {
        super();
        this.message = message;
        this.rowsUploaded = rowsUploaded;
    }

    public String getMessage() {
        return message;
    }

    public int getRowsUploaded() {
        return rowsUploaded;
    }
}