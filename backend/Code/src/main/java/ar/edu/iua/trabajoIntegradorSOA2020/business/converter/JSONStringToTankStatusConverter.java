package ar.edu.iua.trabajoIntegradorSOA2020.business.converter;

import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@ReadingConverter
public class JSONStringToTankStatusConverter implements Converter<String, TankStatus> {

    @Override
    public TankStatus convert(String jsonString) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(jsonString, TankStatus.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}