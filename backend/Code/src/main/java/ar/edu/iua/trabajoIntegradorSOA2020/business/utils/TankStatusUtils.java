package ar.edu.iua.trabajoIntegradorSOA2020.business.utils;

import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.LevelTankRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.TankStatusRepository;
import ar.edu.iua.trabajoIntegradorSOA2020.persistence.VcfRepository;

import java.math.BigDecimal;
import java.util.Optional;

public class TankStatusUtils {
    public static TankStatus completeTankStatusAttributes(TankStatus tankStatus, TankStatusRepository tankStatusDao, LevelTankRepository levelTankDao, VcfRepository tankVcfDao) {
        float productLevel = tankStatus.getLevel() - tankStatus.getWaterLevel();
        tankStatus.setProductLevel(productLevel);

        float tov = levelTankDao.levelToVol(tankStatus.getLevel(), tankStatus.getTank().getIdTank());
        tankStatus.setTov(tov);

        float waterVol = levelTankDao.levelToVol(tankStatus.getWaterLevel(), tankStatus.getTank().getIdTank());
        tankStatus.setWaterVol(waterVol);

        float gov = tankStatus.getTov() - tankStatus.getWaterVol();
        tankStatus.setGov(gov);

        tankStatus.setTll(tankStatus.getTank().getTll());
        tankStatus.setTlv(tankStatus.getTank().getTlv());

        float loadPercent = tankStatus.getTov() * 100 / tankStatus.getTlv();
        tankStatus.setLoadPercent(loadPercent);

        float availableRoom = tankStatus.getTlv() - tankStatus.getTov();
        tankStatus.setAvailableRoom(availableRoom);

        BigDecimal vcf = tankVcfDao.calculateVcf(tankStatus.getDensity(), tankStatus.getTemperature(), tankStatus.getTank().getIdTank());
        BigDecimal gsv = new BigDecimal(tankStatus.getGov()).multiply(vcf);
        tankStatus.setGsv(gsv);

        String levelAlarmStatus = TankLevelAlarmStatusUtils.setLevelAlarmStatus(tankStatus);
        tankStatus.setLevelAlarmStatus(levelAlarmStatus);

        boolean temperatureAlarm = tankStatus.getTemperature().compareTo(tankStatus.getTank().getMaxTemp()) > 0;
        tankStatus.setTemperatureAlarm(temperatureAlarm);

        Optional<TankStatus> tankStatusOptional = tankStatusDao.searchLastTankStatusById(tankStatus.getTank().getIdTank());

        if (!tankStatusOptional.isPresent()) {
            tankStatus.setDeltaProductLevel(0f);
            tankStatus.setDeltaTemperature(new BigDecimal(0).setScale(1, BigDecimal.ROUND_CEILING));
            tankStatus.setDeltaWaterLevel(0f);
        } else {
            TankStatus lastTankStatusSaved = tankStatusOptional.get();

            float deltaProductLevel = tankStatus.getProductLevel() - lastTankStatusSaved.getProductLevel();
            tankStatus.setDeltaProductLevel(deltaProductLevel);

            BigDecimal deltaTemperature = tankStatus.getTemperature().subtract(lastTankStatusSaved.getTemperature());
            tankStatus.setDeltaTemperature(deltaTemperature);

            float deltaWaterLevel = tankStatus.getWaterLevel() - lastTankStatusSaved.getWaterLevel();
            tankStatus.setDeltaWaterLevel(deltaWaterLevel);
        }

        return tankStatus;
    }
}