package ar.edu.iua.trabajoIntegradorSOA2020.security;

public class SecurityConstants {
    static final String SECRET_SIGNATURE = "clave-segura-123";
    //  Tiempo de expiracion en milisegundos
    static final int TOKEN_LIFE_TIME = 3600000;
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String BEARER_TOKEN_PREFIX = "Bearer";
}