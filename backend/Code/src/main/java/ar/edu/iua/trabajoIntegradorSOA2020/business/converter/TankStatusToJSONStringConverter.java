package ar.edu.iua.trabajoIntegradorSOA2020.business.converter;

import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

@Component
@WritingConverter
public class TankStatusToJSONStringConverter implements Converter<TankStatus, String> {

    @Override
    public String convert(TankStatus ts) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(ts);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}