package ar.edu.iua.trabajoIntegradorSOA2020.web.services;

import ar.edu.iua.trabajoIntegradorSOA2020.business.IUserBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.ApiResponseMessage;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TableOfTankUploadedDetails;
import ar.edu.iua.trabajoIntegradorSOA2020.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(RESTConstants.URL_USERS)
@Api(tags = "Users")
public class UserRESTController {

    @Autowired
    private IUserBusiness userBusiness;

    @ApiOperation(value = "Create a new user", authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_CREATED, message = "Created"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_BAD_REQUEST, message = "Bad Request"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(value = "", consumes = RESTConstants.CONTENT_TYPE_JSON, produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<ApiResponseMessage> saveUser(@RequestBody @Valid User user, UriComponentsBuilder uriComponentsBuilder, HttpServletRequest request, BindingResult bindingResult) {
        try {
            //  Revisar que los datos de usuario se hayan pasado bien
            if (bindingResult.hasErrors()) {
                throw new MethodArgumentNotValidException(
                    new MethodParameter(this.getClass().getDeclaredMethod("saveUser", User.class, HttpServletRequest.class, BindingResult.class), 0),
                    bindingResult);
            }

            //  Guardar usuario en bd
            User newUser = userBusiness.create(user);

            //  Retornar Id del usuario creado por cabecera y json
            URI locationURI = uriComponentsBuilder
                    .path(RESTConstants.URL_USERS + "/{id}")
                    .buildAndExpand(newUser.getId())
                    .toUri();

            String message = "The user with id = " + newUser.getId() + " was created successfully.";

            ApiResponseMessage responseMessage = new ApiResponseMessage(message);

            return ResponseEntity.created(locationURI).body(responseMessage);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        } catch (MethodArgumentNotValidException e) {
            return new CustomizedResponseEntityExceptionHandler().handleMethodArgumentNotValidException(e, request);
        } catch (NoSuchMethodException | SecurityException e) {
           return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }


    @ApiOperation(value = "Modify user password", authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_BAD_REQUEST, message = "Bad Request"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @PutMapping(value = "/clave", produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<ApiResponseMessage> modifyPassword(@RequestParam("nuevaClave") String nPassword, @RequestHeader("Authorization") String auth, HttpServletRequest request) {
        try {
            //  Pedir a Business que actualice el usuario en funcion del
            //  token y de la nueva clave
            User user = userBusiness.changePassword(auth, nPassword);

            //  Mensaje de aviso
            String message = "The password of the user " + user.getUsername() + " was changed successfully.";

            //  Enviar respuesta con mensaje en body
            ApiResponseMessage responseMessage = new ApiResponseMessage(message);

            return ResponseEntity.ok(responseMessage);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }
}