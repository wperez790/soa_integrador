package ar.edu.iua.trabajoIntegradorSOA2020.business;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatus;
import ar.edu.iua.trabajoIntegradorSOA2020.model.TankStatusOriginData;

import java.util.List;

public interface ITankStatusBusiness {
    TankStatus save(int idTank, TankStatusOriginData tankStatusOriginData) throws BusinessException, NotFoundException;
    List<TankStatus> getAllLast() throws BusinessException, NotFoundException;
    TankStatus getLast(int id) throws BusinessException, NotFoundException;
}