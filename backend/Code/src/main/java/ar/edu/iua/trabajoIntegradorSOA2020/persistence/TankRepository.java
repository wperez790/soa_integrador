package ar.edu.iua.trabajoIntegradorSOA2020.persistence;

import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface TankRepository extends JpaRepository<Tank, Integer> {
    @Modifying
    @Transactional
    @Query(value = "update tank set tlv = ?1 where id_tank = ?2", nativeQuery = true)
    void updateTankTlv(float tlv, int idTank);

    @Query(value = "SELECT id_tank FROM tank", nativeQuery = true)
    List<Integer> getAllTanksId();
}