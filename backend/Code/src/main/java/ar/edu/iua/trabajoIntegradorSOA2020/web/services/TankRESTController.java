package ar.edu.iua.trabajoIntegradorSOA2020.web.services;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import ar.edu.iua.trabajoIntegradorSOA2020.business.ITankBusiness;
import ar.edu.iua.trabajoIntegradorSOA2020.model.Tank;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.validation.BindingResult;

import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.BusinessException;
import ar.edu.iua.trabajoIntegradorSOA2020.business.exception.NotFoundException;

@RestController
@RequestMapping(RESTConstants.URL_TANKS)
@Api(tags = "Tanks")
public class TankRESTController {

    @Autowired
    private ITankBusiness tankBusiness;

    @ApiOperation(value = "Create a new tank", response = Tank.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_CREATED, message = "Created"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_BAD_REQUEST, message = "Bad Request"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = RESTConstants.CONTENT_TYPE_JSON, produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<Tank> saveTank(@RequestBody @Valid Tank tank, UriComponentsBuilder uriComponentsBuilder, HttpServletRequest request, BindingResult bindingResult) {
        try {
            //  Revisar que los datos de tanque se hayan pasado bien
            if (bindingResult.hasErrors()) {
                throw new MethodArgumentNotValidException(
                    new MethodParameter(this.getClass().getDeclaredMethod("saveTank", Tank.class, UriComponentsBuilder.class, HttpServletRequest.class, BindingResult.class), 0),
                    bindingResult);
            }
            Tank tankCreated = tankBusiness.addTank(tank);

            URI locationURI = uriComponentsBuilder
                    .path(RESTConstants.URL_TANKS + "/{id}")
                    .buildAndExpand(tankCreated.getIdTank())
                    .toUri();

            return ResponseEntity.created(locationURI).body(tankCreated);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        } catch (MethodArgumentNotValidException e) {
            return new CustomizedResponseEntityExceptionHandler().handleMethodArgumentNotValidException(e, request);
        } catch (NoSuchMethodException | SecurityException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }


    @ApiOperation(value = "Get tank by id", response = Tank.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_NOT_FOUND, message = "Not Found"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/{id}", produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<Tank> getTank(@PathVariable("id") int id, HttpServletRequest request) {
        try {
            return ResponseEntity.ok(tankBusiness.getOne(id));
        } catch (NotFoundException e) {
            return new CustomizedResponseEntityExceptionHandler().handleNotFoundException(e, request);
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }


    @ApiOperation(value = "Modify tank by id", response = Tank.class, authorizations = { @Authorization(value="JWT Token") })
    @ApiResponses(value = {
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_OK, message = "Ok"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_BAD_REQUEST, message = "Bad Request"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_UNAUTHORIZED, message = "Unauthorized"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_FORBIDDEN, message = "Forbidden"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_NOT_FOUND, message = "Not Found"),
        @ApiResponse(code = RESTConstants.HTTP_RESPONSE_INTERNAL_SERVER_ERROR, message = "Internal Server Error"),
    })
    @ResponseStatus(value = HttpStatus.OK)
    @PutMapping(value = "/{id}", consumes = RESTConstants.CONTENT_TYPE_JSON, produces = RESTConstants.CONTENT_TYPE_JSON)
    public ResponseEntity<Tank> modifyTank(@PathVariable("id") int id, @RequestBody @Valid Tank tank, HttpServletRequest request, BindingResult bindingResult) {
        try {
            //  Revisar que los datos de tanque se hayan pasado bien
            if (bindingResult.hasErrors()) {
                throw new MethodArgumentNotValidException(
                    new MethodParameter(this.getClass().getDeclaredMethod("modifyTank", Tank.class, HttpServletRequest.class, BindingResult.class), 0),
                    bindingResult);
            }
            return ResponseEntity.ok(tankBusiness.modifyTank(id, tank));
        } catch (BusinessException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        } catch (MethodArgumentNotValidException e) {
            return new CustomizedResponseEntityExceptionHandler().handleMethodArgumentNotValidException(e, request);
        } catch (NoSuchMethodException | SecurityException e) {
            return new CustomizedResponseEntityExceptionHandler().handleAllExceptions(e, request);
        }
    }

}