/*Functions*/

set global log_bin_trust_function_creators = 1;
/* Funcion que calcula el volumen interpolado segun la tabla */
DELIMITER //
drop function if exists levelToVol //
create function levelToVol(level_ float, idtank int) RETURNS FLOAT
BEGIN
declare vol_intable_min FLOAT;
declare level_intable_min FLOAT;
declare vol_intable_max FLOAT;
declare level_intable_max FLOAT;
declare vol_interpoled FLOAT;

set vol_intable_min = (SELECT volume v_min from level_tank where `level` <= level_ and tank_id = idtank order by `level` DESC limit 1);
set level_intable_min = (SELECT `level` l_min from level_tank where `level` <= level_ and tank_id = idtank order by `level` DESC limit 1);
set vol_intable_max = (SELECT volume v_max from level_tank where `level` >= level_ and tank_id = idtank order by `level` limit 1);
set level_intable_max = (SELECT `level` l_max from level_tank where `level` >= level_ and tank_id = idtank order by `level` limit 1);
IF (level_intable_max = 0) THEN
	set level_intable_max = 0.001;
END IF;
IF (level_intable_min = 0) THEN
	set level_intable_min = 0.001;
END IF;
IF (level_ - level_intable_min) > (level_intable_max - level_) THEN
  set vol_interpoled = level_ * vol_intable_max / level_intable_max;
ELSE
  set vol_interpoled = level_ * vol_intable_min / level_intable_min;
END IF;

return vol_interpoled;
END//
DELIMITER ;


DELIMITER //
drop function if exists calculateVcf //
create function calculateVcf(dens decimal(4,3), temp decimal(4,1), idtank int) RETURNS decimal(5,4)
BEGIN

declare vcf_found DECIMAL(5,4);
declare temp_table decimal(4,1);
declare dens_table decimal(4,3);


set temp_table = (SELECT 
    temperature
FROM
    ((SELECT 
        temperature, ABS(temp - temperature) AS diff
    FROM
        vcf
    WHERE
        temperature < temp AND tank_id = idtank
    ORDER BY temperature DESC
    LIMIT 1) UNION ALL (SELECT 
        temperature, ABS(temp - temperature) AS diff
    FROM
        vcf
    WHERE
        temperature >= temp AND tank_id = idtank
    ORDER BY temperature
    LIMIT 1)) AS tmp
ORDER BY diff
LIMIT 1);

set dens_table = (SELECT 
    density
FROM
    ((SELECT 
        density, ABS(dens - density) AS diff
    FROM
        vcf
    WHERE
        density < dens AND tank_id = idtank
    ORDER BY density DESC
    LIMIT 1) UNION ALL (SELECT 
        density, ABS(dens - density) AS diff
    FROM
        vcf
    WHERE
        density >= dens AND tank_id = idtank
    ORDER BY density
    LIMIT 1)) AS tmp2
ORDER BY diff
LIMIT 1);

set vcf_found = (SELECT 
    vcf vcf_found
FROM
    vcf
WHERE
    density LIKE dens_table
        AND temperature LIKE temp_table
        AND tank_id = idtank);

return vcf_found;

END//
DELIMITER ;
