#include <SPI.h>
#include <Ethernet.h>
#include <Modbus.h>
#include <ModbusIP.h>
#include <dht11.h>

//Pines
#define DHT11_PIN 5
#define NIVTANQUE_PIN A0
#define CANTFLUIDO_PIN A1
#define DENSIDAD_PIN  A2

//Registros MB
#define TEMP 0 
#define HUM 1 
#define NIVTANQUE 2
#define CANTFLUIDO 3
#define DENS 4
#define DENS_DEC 5

#define MAX_VALUE_AN 1023.0
#define MAX_LEVEL 1000.0
#define MAX_DEN 1.1
#define MIN_DEN 0.65

//Variables
float temp;
float hum;
long nivelTanque = 0;
long cantidadFluido = 0;
float densidad = 0;
int densidadDEC = 0;

// ModbusIP object
ModbusIP mb;
// Sensor object
dht11 DHT11; 

void setup() {
    // The media access control (ethernet hardware) address for the shield
    byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  
    // The IP address for the shield
    byte ip[] = { 192, 168, 100, 25 };   
    // Config Modbus IP 
    mb.config(mac, ip);
    // Add SERVO_HREG register - Use addHreg() for analog outpus or to store values in device 
    mb.addHreg(TEMP, 0);
    mb.addHreg(HUM, 0);
    mb.addHreg(NIVTANQUE, 0);
    mb.addHreg(CANTFLUIDO, 0);
    mb.addHreg(DENS, 0);
    mb.addHreg(DENS_DEC, 0);

    Serial.begin(9600);
    Serial.print("\nInit\n");
}

void loop() {
   //Call once inside loop() - all magic here
   mb.task();

   //DHT11
   int chk = DHT11.read(DHT11_PIN);
   temp = DHT11.temperature;
   hum = DHT11.humidity;

   delay(10);

   nivelTanque = (analogRead(NIVTANQUE_PIN)/MAX_VALUE_AN)*MAX_LEVEL;
   cantidadFluido = (analogRead(CANTFLUIDO_PIN)/MAX_VALUE_AN) * nivelTanque;
   densidad = (analogRead(DENSIDAD_PIN)/MAX_VALUE_AN)*(MAX_DEN-MIN_DEN)+MIN_DEN;
   densidadDEC = int(densidad*100)%100;
   
   
   //Attach switchPin to SWITCH_ISTS register     
   mb.Hreg(TEMP, temp);
   mb.Hreg(HUM, hum);
   mb.Hreg(NIVTANQUE, nivelTanque);
   mb.Hreg(CANTFLUIDO, cantidadFluido);
   mb.Hreg(DENS, densidad);
   mb.Hreg(DENS_DEC, densidadDEC);
   delay(15);
}
