﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using System.Collections.Specialized;
using System;

public class GET_Data : MonoBehaviour
{
    float nextActionTime;
    float period;
    public int id;
    public TextMesh level;
    public TextMesh data;
    string token;
    public GameObject water;
    private Vector3 scale;

    // Start is called before the first frame update
    void Start()
    {
        nextActionTime = 0.0f;
        period = 5.0f;
        StartCoroutine(GetToken());
        scale = water.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            // execute block of code here
            StartCoroutine(GetDataFromTank(id));
        }

    }
    
    IEnumerator GetDataFromTank(int id)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get("https://soa-practica-pp.mooo.com/backend/tanques/"+id+"/estado"))
        {
            // Request and wait for the desired page.
            webRequest.SetRequestHeader("Authorization", token);
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(id + ": ID Error: " + webRequest.error);
            }
            else
            {
                JSONNode jsnode = JSON.Parse(webRequest.downloadHandler.text);
                data.text = webRequest.downloadHandler.text;
                data.text = "Tanque " + id + "\n"
                    + "Temperatura: " + jsnode["temperature"] + " ºC"+ "\n"
                    + "Volumen: " + jsnode["tov"] + " l" +"\n"
                    + "Densidad: " + jsnode["density"] + " kg/mm3" + "\n";
                level.text = Math.Round((float) jsnode["loadPercent"], 2) + " %";
                scale.y = 0.095f * jsnode["loadPercent"] / 100.0f;
                water.transform.localScale = new Vector3(scale.x, scale.y, scale.z);
            }
        }
    }
    IEnumerator GetToken()
    {
        //USER DATA
        JSONObject jsonData = new JSONObject();
        jsonData["username"] = "arduino";
        jsonData["password"] = "claveDeArduino123";

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jsonData.ToString());
        
        using (UnityWebRequest webRequest = UnityWebRequest.Post("https://soa-practica-pp.mooo.com/backend/login", ""))
        {
            webRequest.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
            webRequest.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            webRequest.SetRequestHeader("Content-Type", "application/json");
            // Request and wait for the desired page.
            // webRequest.SetRequestHeader("Accept", "application/json");
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log("Error: " + webRequest.error);
            }
            else
            {
                JSONNode jsnode = JSON.Parse(webRequest.downloadHandler.text);
                token = "Bearer " + jsnode["access_token"];

            }

        }
    }
}
